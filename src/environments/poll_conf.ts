export const poll_conf = {
	defaultConfig: {
		maxCountOfAnswers: 150,
		expiresDaysDelay: 60,
		expiracyAfterLastModificationInDays: 180,
		whoCanChangeAnswers: 'everybody',
		visibility: 'link_only',
		voteChoices: 'only_yes',
	},
};
