import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollResultsDetailedComponent } from './poll-results-detailed.component';

describe('PollResultsDetailedComponent', () => {
	let component: PollResultsDetailedComponent;
	let fixture: ComponentFixture<PollResultsDetailedComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PollResultsDetailedComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PollResultsDetailedComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
