import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
	public env = environment;
	show_bottom_sources: boolean = environment.show_bottom_sources;
	constructor() {}

	ngOnInit(): void {}

	subscribeToNewsletter() {
		alert('TODO');
	}
}
